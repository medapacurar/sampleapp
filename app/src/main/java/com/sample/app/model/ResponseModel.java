package com.sample.app.model;

/**
 * Created by Meda on 4/21/2017.
 */

public class ResponseModel {

    private boolean success;
    private String message;
    private String next;

    public ResponseModel(boolean success, String message, String next, String dataUrl) {
        this.success = success;
        this.message = message != null ? message : "";
        this.next = next != null ? next : "";
    }

    public boolean isSuccess() {
        return success;
    }

    public String getNext() {
        return next;
    }

}
