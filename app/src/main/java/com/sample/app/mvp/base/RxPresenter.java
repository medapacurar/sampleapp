package com.sample.app.mvp.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

public class RxPresenter<V> {

    private final V mView;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private final BehaviorSubject<Boolean> mViewReady = BehaviorSubject.createDefault(false);

    protected RxPresenter(final V view) {
        mView = view;
    }

/*
 * @return the view of this presenter
*/

    protected V getView() {
        return mView;
    }

/*
 * add your compositeSubscription for View events to this method to get them automatically cleaned up in
 * {@link #sleep()}. typically call this in {@link #wakeUp()} where you subscribe to the UI
 * events
 * */

    protected void manageViewDisposables(final Disposable disposable) {
        compositeDisposable.add(disposable);
    }

/*
 * completes all observables of this presenter. Should be called when the view is about to die
 * and will never come back.
 * <p/>
 * call this in {@link Fragment#onDestroy()}
 * <p/>
 * complete all {@link Observer}, i.e. BehaviourSubjects with {@link Observer#onCompleted()}
 * to unsubscribe all observers
 */

    public void destroy() {
        mViewReady.onNext(false);
    }

/*
 * call sleep as the opposite of {@link #wakeUp()} to unsubscribe all observers listening to the
 * UI observables of the view. Calling sleep in {@link Fragment#onDestroyView()} makes sense
 * because observing a discarded view does not.
*/

    public void sleep() {
        mViewReady.onNext(false);
        // unsubscribe all UI compositeSubscription created in wakeUp() and added
        // via manageViewDisposables(Subscription)
        compositeDisposable.dispose();
        // there is no reuse possible. recreation works fine
        compositeDisposable = new CompositeDisposable();
    }

}
