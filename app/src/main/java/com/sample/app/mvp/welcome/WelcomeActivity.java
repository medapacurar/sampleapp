package com.sample.app.mvp.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.sample.app.R;
import com.sample.app.adapter.PagerAdapterMain;
import com.sample.app.mvp.register.RegisterActivity;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity {
    @BindView(R.id.welcome_pager)
    ViewPager pager;
    @BindView(R.id.page_indicator)
    CirclePageIndicator pageIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        PagerAdapterMain pAdapter = new PagerAdapterMain(getSupportFragmentManager());
        pager.setAdapter(pAdapter);
        pageIndicator.setViewPager(pager);
    }

    @OnClick(R.id.btn_start)
    void startClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
