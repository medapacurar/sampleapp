package com.sample.app.mvp.register.email;

import com.sample.app.mvp.base.BaseView;

/**
 * Created by Meda on 5/3/2017.
 */

interface EmailValidationView extends BaseView {

    void continueButtonEnable(boolean isEnable);

    void openProfilePage();
}
