package com.sample.app.mvp.register.email;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.model.ConfirmCode;
import com.sample.app.model.ResponseModel;
import com.sample.app.mvp.base.RxPresenter;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Meda on 5/3/2017.
 */

class EmailValidationPresenter extends RxPresenter<EmailValidationView> {
    final static String PROFILE = "AskToVerifyProfile";

    private ApiService apiService;

    EmailValidationPresenter(EmailValidationView view, ApiService apiService) {
        super(view);
        this.apiService = apiService;
    }

    /**
     * check if the email has the correct format
     *
     * @param s - email address
     */
    void validateEmail(String s) {
        if (s != null) {
            getView().continueButtonEnable(android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches());
        }
    }

    /**
     * submit the email address introduced by the user
     *
     * @param phoneNumber - user phone number
     * @param email       - the introduced email
     * @param confirmCode - received confirmation code
     */
    void submitEmailAddress(String phoneNumber, String email, String confirmCode) {
        ConfirmCode confirm = new ConfirmCode(phoneNumber, email, confirmCode);
        apiService.confirmPhoneNumber(confirm)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        manageViewDisposables(d);
                    }

                    @Override
                    public void onNext(Response<ResponseModel> value) {
                        if (value.isSuccessful() && value.body() != null && value.body().isSuccess()) {
                            if (value.body().getNext().equals(PROFILE)) {
                                getView().openProfilePage();
                            }
                        } else {
                            getView().notificationMessage(android.R.drawable.ic_dialog_alert,
                                    R.string.error);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().notificationMessage(android.R.drawable.ic_dialog_alert,
                                R.string.error);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
