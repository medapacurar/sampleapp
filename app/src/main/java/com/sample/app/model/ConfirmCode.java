package com.sample.app.model;

/**
 * Created by Meda on 4/28/2017.
 */

public class ConfirmCode {

    private String phoneNumber;
    private String emailAddress;
    private String confirmCode;

    public ConfirmCode(String phoneNumber, String emailAddress, String confirmCode) {
        this.phoneNumber = phoneNumber != null ? phoneNumber : "";
        this.emailAddress = emailAddress != null ? emailAddress : "";
        this.confirmCode = confirmCode != null ? confirmCode : "";
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getConfirmCode() {
        return confirmCode;
    }
}
