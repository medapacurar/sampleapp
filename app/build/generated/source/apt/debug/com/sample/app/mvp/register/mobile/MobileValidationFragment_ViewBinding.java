// Generated code from Butter Knife. Do not modify!
package com.sample.app.mvp.register.mobile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sample.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MobileValidationFragment_ViewBinding implements Unbinder {
  private MobileValidationFragment target;

  private View view2131558545;

  @UiThread
  public MobileValidationFragment_ViewBinding(final MobileValidationFragment target, View source) {
    this.target = target;

    View view;
    target.mobileNumberInput = Utils.findRequiredViewAsType(source, R.id.mobile_number_input, "field 'mobileNumberInput'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_submit_number, "field 'submitBtn' and method 'submitClicked'");
    target.submitBtn = Utils.castView(view, R.id.btn_submit_number, "field 'submitBtn'", Button.class);
    view2131558545 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submitClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MobileValidationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mobileNumberInput = null;
    target.submitBtn = null;

    view2131558545.setOnClickListener(null);
    view2131558545 = null;
  }
}
