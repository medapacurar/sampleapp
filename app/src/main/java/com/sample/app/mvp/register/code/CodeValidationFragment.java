package com.sample.app.mvp.register.code;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.api.RetrofitApi;
import com.sample.app.mvp.register.email.EmailValidationFragment;
import com.sample.app.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */

public class CodeValidationFragment extends Fragment implements CodeValidationView {

    public static final String CONFIRMATION_CODE = "CONFIRM";
    public static final String MOBILE_NUMBER = "mobile_number";

    private Unbinder unbinder;
    private CodeValidationPresenter presenter;
    private ApiService apiServices;
    private String confirmationCode, mobileNumber;
    @BindView(R.id.code_input)
    EditText inputCode;
    @BindView(R.id.code_submit)
    Button submit;
    private Context context;

    public CodeValidationFragment() {
        // Required empty public constructor
    }

    public static CodeValidationFragment newInstance(String confirmationCode, String mobileNumber) {
        Bundle args = new Bundle();
        args.putString(CONFIRMATION_CODE, confirmationCode);
        args.putString(MOBILE_NUMBER, mobileNumber);
        CodeValidationFragment fragment = new CodeValidationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        apiServices = RetrofitApi.createService(ApiService.class);
        confirmationCode = getArguments().getString(CONFIRMATION_CODE);
        mobileNumber = getArguments().getString(MOBILE_NUMBER);
        if (presenter == null) {
            presenter = new CodeValidationPresenter(this, apiServices);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_code_validation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        inputCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.textListener(inputCode.getText().toString());
            }
        });

        inputCode.setText(confirmationCode);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.sleep();
        }
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter == null) {
            presenter = new CodeValidationPresenter(this, apiServices);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.destroy();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.destroy();
        }
    }


    @Override
    public void notificationMessage(int resourceIcon, int resourceText) {
        Utils.createDialog(context, getString(resourceText), R.string.dialog_error, resourceIcon)
                .show();
    }

    @Override
    public void openProfilePage() {
      //TODO: go to profile
    }

    @Override
    public void openOrganizationsPage() {
   //TODO: go to organizations
    }

    @Override
    public void openEmailPage() {
        Utils.loadFragment(R.id.activity_register, getFragmentManager(), EmailValidationFragment.newInstance(mobileNumber, confirmationCode), true,
                EmailValidationFragment.class.getSimpleName());
    }

    @OnClick(R.id.code_submit)
    void submitPressed() {
        presenter.sendConfirmCode(mobileNumber, inputCode.getText().toString());
    }

    @Override
    public void continueButtonEnable(boolean isEnable) {
        submit.setBackground(ContextCompat.getDrawable(context,
                isEnable ? R.drawable.button_enabled_background : R.drawable.button_disabled_background));

        submit.setTextColor(ContextCompat.getColor(context,
                isEnable ? R.color.white : R.color.transparent_white));

        submit.setClickable(isEnable);
    }
}
