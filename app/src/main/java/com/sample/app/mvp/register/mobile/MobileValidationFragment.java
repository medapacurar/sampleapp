package com.sample.app.mvp.register.mobile;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;
import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.api.RetrofitApi;
import com.sample.app.mvp.register.code.CodeValidationFragment;
import com.sample.app.utils.Globals;
import com.sample.app.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.sample.app.utils.Globals.APP_ID_KEY;
import static com.sample.app.utils.Globals.DEFAULT_ENCRYPTION_KEY;
import static com.sample.app.utils.Globals.ENCRYPTION_KEY;

/**
 * A simple {@link Fragment} subclass.
 */

public class MobileValidationFragment extends Fragment implements MobileValidationView {
    private Unbinder unbinder;
    private MobileValidationPresenter presenter;

    @BindView(R.id.mobile_number_input)
    EditText mobileNumberInput;
    @BindView(R.id.btn_submit_number)
    Button submitBtn;
    private Context context;
    private Activity activity;
    private ApiService apiServices;
    private ApiService basicAuthServices;

    public MobileValidationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = getActivity();
        // Hawk.put(APP_ID_KEY, RandomStringUtils.randomNumeric(22));
        Hawk.put(APP_ID_KEY, "8976396489281405505863");
//        Hawk.put(APP_ID_KEY, "8976396489281405505863");
        Hawk.put(ENCRYPTION_KEY, DEFAULT_ENCRYPTION_KEY);
        apiServices = RetrofitApi.createService(ApiService.class);
        if (presenter == null) {
            presenter = new MobileValidationPresenter(this, apiServices);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_validation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        Utils.hideKeyboard(view, activity);
        mobileNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.validateTextChanges(mobileNumberInput.getText().toString());
            }
        });
    }

    @OnClick(R.id.btn_submit_number)
    void submitClicked() {
        submit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.sleep();
        }
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter == null) {
            presenter = new MobileValidationPresenter(this, apiServices);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.destroy();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.destroy();
        }
    }


    @Override
    public void submit() {
        presenter.viewDetails(mobileNumberInput.getText().toString());
        // submit to server - go to next screen
    }

    @Override
    public void openCodeConfirmation(String confirmationCode) {
        Utils.loadFragment(R.id.activity_register, getFragmentManager(), CodeValidationFragment.newInstance
                        (confirmationCode, mobileNumberInput.getText().toString()), true,
                CodeValidationFragment.class.getSimpleName());
    }

    @Override
    public void startButtonEnable(boolean isEnable) {
        submitBtn.setBackground(ContextCompat.getDrawable(context,
                isEnable ? R.drawable.button_enabled_background : R.drawable.button_disabled_background));

        submitBtn.setTextColor(ContextCompat.getColor(context,
                isEnable ? R.color.white : R.color.transparent_white));
    }

    @Override
    public void notificationMessage(int resourceIcon, int resourceText) {
        Utils.createDialog(context, getString(resourceText), R.string.dialog_error, resourceIcon)
                .show();
    }

    @Override
    public void updateSubmitButton(int selection, String text) {
        mobileNumberInput.setText(text);
        mobileNumberInput.setSelection(selection);
    }

    @Override
    public void storeKeys(String encryptionKey, String appIda) {
        Hawk.put(Globals.ENCRYPTION_KEY, encryptionKey);
        Hawk.put(Globals.APP_ID_KEY, appIda);
    }

    @Override
    public ApiService buildApi() {
        return RetrofitApi.createService(ApiService.class);
    }
}
