package com.sample.app.mvp.welcome;

import com.sample.app.mvp.base.RxPresenter;

/**
 * Created by Meda on 4/12/2017.
 */

public class WelcomePresenter extends RxPresenter<WelcomeView>{

    protected WelcomePresenter(WelcomeView view) {
        super(view);
    }
}
