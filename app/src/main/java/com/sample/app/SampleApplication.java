package com.sample.app;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * Created by Meda on 4/13/2017.
 */

public class SampleApplication extends MultiDexApplication {

    private Picasso picasso;
    private static SampleApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = this;
        sInstance = this;
        setUpHawk();

        Timber.plant(new Timber.DebugTree());

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.i(message);
            }
        });

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();

    }

    private void setUpHawk() {
        Hawk.init(this).build();
    }

    public static SampleApplication get(Activity activity) {
        return (SampleApplication) activity.getApplication();
    }

    public static SampleApplication getInstance() {
        return sInstance;
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
