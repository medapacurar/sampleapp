package com.sample.app.utils;

import com.squareup.okhttp.Credentials;

/**
 * Created by Meda on 4/28/2017.
 */

public class Globals {

    public static final String DEFAULT_ENCRYPTION_KEY = "fc3645ba-b7ed-4b4b-aef7";
    public static final String APP_ID_KEY = "app_id";
    public static final String ENCRYPTION_KEY = "encryption_key";

}
