package com.sample.app.adapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sample.app.mvp.welcome.WelcomeFragment;

/**
 * Created by user on 8/11/2015.
 */
public class PagerAdapterMain extends FragmentPagerAdapter {
    private static final int PAGE_NR = 3;

    public PagerAdapterMain(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_NR;
    }

    @Override
    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(WelcomeFragment.FRAGMENT_POSITION, i);
        WelcomeFragment presentationFragment = new WelcomeFragment();
        presentationFragment.setArguments(bundle);
        return presentationFragment;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
