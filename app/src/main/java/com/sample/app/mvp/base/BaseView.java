package com.sample.app.mvp.base;

/**
 * Created by Meda on 4/20/2017.
 */

public interface BaseView<T> {

    void notificationMessage(int resourceIcon, int resourceText);
}
