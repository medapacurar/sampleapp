package com.sample.app.mvp.main;

import com.sample.app.mvp.base.RxPresenter;

/**
 * Created by Meda on 5/11/2017.
 */

class MainPresenter extends RxPresenter<MainView> {
    protected MainPresenter(MainView view) {
        super(view);
    }
}
