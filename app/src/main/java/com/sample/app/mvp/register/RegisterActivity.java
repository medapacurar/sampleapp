package com.sample.app.mvp.register;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sample.app.R;
import com.sample.app.mvp.register.mobile.MobileValidationFragment;
import com.sample.app.utils.Utils;

import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        Utils.loadFragment(R.id.activity_register, getSupportFragmentManager(), new MobileValidationFragment(), true,
                MobileValidationFragment.class.getSimpleName());
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }
}
