package com.sample.app.mvp.register.mobile;

import com.sample.app.api.ApiService;
import com.sample.app.mvp.base.BaseView;

/**
 * Created by Meda on 4/20/2017.
 */

public interface MobileValidationView extends BaseView {

    void submit();

    void openCodeConfirmation(String confirmationCode);

    void startButtonEnable(boolean isEnable);

    void updateSubmitButton(int selection, String text);

    void storeKeys(String encryptionKey, String appIda);

    ApiService buildApi();
}
