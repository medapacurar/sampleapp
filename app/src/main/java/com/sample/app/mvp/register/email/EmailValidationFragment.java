package com.sample.app.mvp.register.email;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.api.RetrofitApi;
import com.sample.app.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.sample.app.mvp.register.code.CodeValidationFragment.CONFIRMATION_CODE;
import static com.sample.app.mvp.register.code.CodeValidationFragment.MOBILE_NUMBER;

/**
 * A simple {@link Fragment} subclass.
 */

public class EmailValidationFragment extends Fragment implements EmailValidationView {
    @BindView(R.id.email_input)
    EditText emailInput;
    @BindView(R.id.email_submit)
    Button submit;

    private EmailValidationPresenter presenter;
    private Context context;
    private ApiService apiServices;
    private Unbinder unbinder;
    private String phoneNumber, confirmationCode;


    public EmailValidationFragment() {
        // Required empty public constructor
    }

    public static EmailValidationFragment newInstance(String phoneNumber, String confirmationCode) {
        Bundle args = new Bundle();
        args.putString(CONFIRMATION_CODE, confirmationCode);
        args.putString(MOBILE_NUMBER, phoneNumber);
        EmailValidationFragment fragment = new EmailValidationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        confirmationCode = getArguments().getString(CONFIRMATION_CODE);
        phoneNumber = getArguments().getString(MOBILE_NUMBER);
        apiServices = RetrofitApi.createService(ApiService.class);
        if (presenter == null) {
            presenter = new EmailValidationPresenter(this, apiServices);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email_validation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.validateEmail(emailInput.getText().toString());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.sleep();
        }
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter == null) {
            presenter = new EmailValidationPresenter(this, apiServices);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.destroy();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.destroy();
        }
    }

    /**
     * create a notification Dialog based on on details received from the presenter
     *
     * @param resourceIcon - resource id for the dialog icon
     * @param resourceText - resource id for the text
     */
    @Override
    public void notificationMessage(int resourceIcon, int resourceText) {
        Utils.createDialog(context, getString(resourceText), R.string.dialog_error, resourceIcon)
                .show();
    }


    @OnClick(R.id.email_submit)
    void submitPressed() {
        presenter.submitEmailAddress(phoneNumber, emailInput.getText().toString(), confirmationCode);
    }


    /**
     * change the background and behaviour of the submit button
     * @param isEnable - if the button should be clickable or not
     */
    @Override
    public void continueButtonEnable(boolean isEnable) {
        submit.setBackground(ContextCompat.getDrawable(context,
                isEnable ? R.drawable.button_enabled_background : R.drawable.button_disabled_background));

        submit.setTextColor(ContextCompat.getColor(context,
                isEnable ? R.color.white : R.color.transparent_white));

        submit.setClickable(isEnable);
    }

    @Override
    public void openProfilePage() {
        //TODO go to profile
    }

}
