package com.sample.app.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Meda on 4/19/2017.
 */

public class Utils {

    /**
     * Load a Fragment
     * @param frame - the frame id where the fragment will be added
     * @param fragmentManager -
     * @param fragment - a new Fragment instance to add
     * @param addToBackStack - if the fragment should be put in the backstack
     * @param tag - the fragment tag
     */
    public static void loadFragment(int frame, FragmentManager fragmentManager, Fragment fragment, boolean
            addToBackStack, String tag) {
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(tag, 0);
        if (!fragmentPopped) {
            FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.replace(frame, fragment);
            if (addToBackStack) {
                trans.addToBackStack(tag);
            }
            trans.commit();
        }
    }

    /**
     * create an AlertDialog
     * @param context - context where to create it
     * @param message - the message to be displayed
     * @param titleRes - resource id for the title
     * @param iconRes - resource id for the icon
     * @return an AlertDialog.Builder - Do not forget to call .show!
     */
    public static AlertDialog.Builder createDialog(Context context, String message, int titleRes, int iconRes) {
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle(titleRes)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {})
                .setIcon(iconRes);
    }

    /**
     * close th keyboard for this activity
     */
    private static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity
                .INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * Hide keyboard if the user touches any part of the screen that is not an EditText
     * @param view - the view that has been touched
     * @param activity - the activity from where it has been touched
     */
    public static void hideKeyboard(View view, final Activity activity) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard(activity);
                return false;
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboard(innerView, activity);
            }
        }
    }
}
