package com.sample.app.api;

import com.sample.app.model.AppDetails;
import com.sample.app.model.ConfirmCode;
import com.sample.app.model.MobileValidation;
import com.sample.app.model.ResponseModel;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Meda on 4/13/2017.
 */

public interface ApiService<T> {

    @PUT("/registerNumber")
    Observable<Response<ResponseModel>> registerPhoneNumber(@Body MobileValidation phoneNumber);

    @POST("/confirmNumber")
    Observable<Response<ResponseModel>> confirmPhoneNumber(@Body ConfirmCode confirmCode);

    @GET("/getDetails")
    Observable<Response<AppDetails>> getDetails(@Query("phoneNumber") String phoneNumber);


}
