package com.sample.app.api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.Locale;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Connection;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Meda on 4/26/2017.
 */

public class RetrofitApi {

    private static GsonBuilder gsonBuilder = new GsonBuilder();
    private static Gson gson = gsonBuilder.create();

    /**
     * creates the OkHttpClien Interceptor
     */
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    try {
                        Request request = chain.request();
                        Request newRequest = request.newBuilder()
                                .addHeader("Authorization", "TOKEN") // TODO: ADD token
                                .build();
                        Response response = chain.proceed(newRequest);
                        String bodyString = logNetworkRequest(newRequest, response, chain.connection());
                        return response
                                .newBuilder()
                                .body(ResponseBody.create(response.body().contentType(), bodyString))
                                .build();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }).build();


    /**
     * creates the retrofit instance
     */
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.sample-test.com")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
            .client(okHttpClient)
            .build();


    /**
     * creates retrofit service
     */
    public static <Retrofit> Retrofit createService(final Class<Retrofit> serviceClass) {
        return retrofit.create(serviceClass);
    }


    /**
     * Detailed logs about the network request
     */
    private static String logNetworkRequest(Request request, Response response, Connection connection) {
        long t1 = System.nanoTime();
        String requestLog = String.format("Sending request %s on %s%n%s", request.url(), connection, request.headers());
        if (request.method().compareToIgnoreCase("post") == 0) {
            requestLog = "\n" + requestLog + "\n" + bodyToString(request);
        }
        Log.d("Request", "request" + "\n" + requestLog);
        long t2 = System.nanoTime();
        String responseLog = String.format(Locale.ENGLISH, "Response  %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers());
        String bodyString = null;
        try {
            bodyString = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Response", "response" + "\n" + responseLog + "\n" + bodyString);
        return bodyString;
    }

    /**
     * Buffering the stream and return a String version
     *
     * @param request - the request that will be buffered
     * @return String version
     */
    private static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "The request body could not be parsed as a String. LoggingInterceptor class - bodyToString()";
        }
    }

}
