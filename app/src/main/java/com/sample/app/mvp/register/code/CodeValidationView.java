package com.sample.app.mvp.register.code;

import com.sample.app.mvp.base.BaseView;

/**
 * Created by Meda on 4/27/2017.
 */

interface CodeValidationView extends BaseView {

    void openProfilePage();

    void openOrganizationsPage();

    void openEmailPage();

    void continueButtonEnable(boolean isEnable);
}
