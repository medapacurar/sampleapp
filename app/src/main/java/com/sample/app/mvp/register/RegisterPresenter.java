package com.sample.app.mvp.register;

import com.sample.app.mvp.base.RxPresenter;

/**
 * Created by Meda on 4/21/2017.
 */

class RegisterPresenter extends RxPresenter<RegisterActivityView> {

    protected RegisterPresenter(RegisterActivityView view) {
        super(view);
    }

}
