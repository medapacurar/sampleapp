package com.sample.app.mvp.register.mobile;

import android.util.Log;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.model.AppDetails;
import com.sample.app.model.MobileValidation;
import com.sample.app.model.ResponseModel;
import com.sample.app.mvp.base.RxPresenter;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Meda on 4/20/2017.
 */

public class MobileValidationPresenter extends RxPresenter<MobileValidationView> {
    private static final String CONFIRMATION_CODE = "AskForConfirmationCode";

    private ApiService apiService;

    public MobileValidationPresenter(MobileValidationView view, ApiService apiService) {
        super(view);
        this.apiService = apiService;
    }

    public void submitNumber(String mobileNumber) {
        apiService.registerPhoneNumber(new MobileValidation(mobileNumber))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        manageViewDisposables(d);
                    }

                    @Override
                    public void onNext(Response<ResponseModel> value) {
                        if (value.isSuccessful() && value.body() != null) {
                            if (value.body().isSuccess() && value.body().getNext().equals(CONFIRMATION_CODE)) {
                                viewDetails(mobileNumber);
                            } else {
                                getView().notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
                            }
                        } else {
                            getView().notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getMessage());
                        getView().notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void viewDetails(String mobileNumber) {
        getView().buildApi().getDetails(mobileNumber)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<AppDetails>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        manageViewDisposables(d);
                    }

                    @Override
                    public void onNext(Response<AppDetails> value) {
                        if (value.isSuccessful() && value.body() != null) {
                            getView().openCodeConfirmation(value.body().getConfirmCode());
                            getView().storeKeys(value.body().getEncryptionKey(), value.body().getAppId());
                        } else {
                            getView().notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        getView().notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void validateTextChanges(String text) {
        int count = text.length();
        if (count == 10) {
            getView().startButtonEnable(true);
        } else {
            getView().startButtonEnable(false);
        }
    }

}
