// Generated code from Butter Knife. Do not modify!
package com.sample.app.mvp.welcome;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sample.app.R;
import com.viewpagerindicator.CirclePageIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WelcomeActivity_ViewBinding implements Unbinder {
  private WelcomeActivity target;

  private View view2131558525;

  @UiThread
  public WelcomeActivity_ViewBinding(WelcomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WelcomeActivity_ViewBinding(final WelcomeActivity target, View source) {
    this.target = target;

    View view;
    target.pager = Utils.findRequiredViewAsType(source, R.id.welcome_pager, "field 'pager'", ViewPager.class);
    target.pageIndicator = Utils.findRequiredViewAsType(source, R.id.page_indicator, "field 'pageIndicator'", CirclePageIndicator.class);
    view = Utils.findRequiredView(source, R.id.btn_start, "method 'startClicked'");
    view2131558525 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.startClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    WelcomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pager = null;
    target.pageIndicator = null;

    view2131558525.setOnClickListener(null);
    view2131558525 = null;
  }
}
