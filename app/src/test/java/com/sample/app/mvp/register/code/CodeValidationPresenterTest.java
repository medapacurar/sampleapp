package com.sample.app.mvp.register.code;

import com.sample.app.api.ApiService;
import com.sample.app.model.ConfirmCode;
import com.sample.app.model.ResponseModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Meda on 5/3/2017.
 */


public class CodeValidationPresenterTest {

    private CodeValidationPresenter presenter;
    @Mock
    private CodeValidationView view;
    @Mock
    private ApiService apiService;
    @Mock
    private ResponseModel responseModel;
    @Mock
    private ResponseBody errorResponse;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        presenter = new CodeValidationPresenter(view, apiService);
    }

    @Test
    public void textListener_CorrectLength_EnableBtn() throws Exception {
        presenter.textListener("123456");
        verify(view).continueButtonEnable(true);
    }

    @Test
    public void textListener_InCorrectLength_DisableBtn() throws Exception {
        presenter.textListener("12345");
        verify(view).continueButtonEnable(false);
    }

    @Test
    public void sendConfirmationCode_SuccessfulResponseWithProfile_OpenProfie() throws Exception {
        when(apiService.confirmPhoneNumber(any(ConfirmCode.class)))
                .thenReturn(Observable.just(Response.success(responseModel)));
        when(responseModel.isSuccess())
                .thenReturn(true);
        when(responseModel.getNext())
                .thenReturn(CodeValidationPresenter.PROFILE);
        presenter.sendConfirmCode("","");
        verify(view).openProfilePage();
    }

    @Test
    public void sendConfirmationCode_SuccessfulResponseWithEmail_OpenEmail() throws Exception {
        when(apiService.confirmPhoneNumber(any(ConfirmCode.class)))
                .thenReturn(Observable.just(Response.success(responseModel)));
        when(responseModel.isSuccess())
                .thenReturn(true);
        when(responseModel.getNext())
                .thenReturn(CodeValidationPresenter.EMAIL);
        presenter.sendConfirmCode("","");
        verify(view).openEmailPage();
    }


    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }


}