// Generated code from Butter Knife. Do not modify!
package com.sample.app.mvp.register.email;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sample.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmailValidationFragment_ViewBinding implements Unbinder {
  private EmailValidationFragment target;

  private View view2131558543;

  @UiThread
  public EmailValidationFragment_ViewBinding(final EmailValidationFragment target, View source) {
    this.target = target;

    View view;
    target.emailInput = Utils.findRequiredViewAsType(source, R.id.email_input, "field 'emailInput'", EditText.class);
    view = Utils.findRequiredView(source, R.id.email_submit, "field 'submit' and method 'submitPressed'");
    target.submit = Utils.castView(view, R.id.email_submit, "field 'submit'", Button.class);
    view2131558543 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submitPressed();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmailValidationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.emailInput = null;
    target.submit = null;

    view2131558543.setOnClickListener(null);
    view2131558543 = null;
  }
}
