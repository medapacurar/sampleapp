package com.sample.app.mvp.register.code;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.model.ConfirmCode;
import com.sample.app.model.ResponseModel;
import com.sample.app.mvp.base.RxPresenter;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Meda on 4/27/2017.
 */

class CodeValidationPresenter extends RxPresenter<CodeValidationView> {
    final static String PROFILE = "AskToVerifyProfile";
    final static String EMAIL = "AskForEmailAddress";

    private ApiService apiService;

    CodeValidationPresenter(CodeValidationView view, ApiService apiService) {
        super(view);
        this.apiService = apiService;
    }

    /**
     * checks the length of the confirmation code and enables the button if the length is correct
     *
     * @param text - input text
     */
    void textListener(String text) {
        getView().continueButtonEnable(text.length() == 6);
    }

    /**
     * sends to server the confirmation code introduced by the user
     * @param phoneNumber - phone number introduced in the previous screen
     * @param confirmationCode - received confirmation code
     */
    void sendConfirmCode(String phoneNumber, String confirmationCode) {
        ConfirmCode confirmCode = new ConfirmCode(phoneNumber, null, confirmationCode);
        apiService.confirmPhoneNumber(confirmCode)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        manageViewDisposables(d);
                    }

                    @Override
                    public void onNext(Response<ResponseModel> value) {
                        if (value.isSuccessful() && value.body() != null && value.body().isSuccess()) {
                            switch (value.body().getNext()) {
                                case PROFILE:
                                    getView().openProfilePage();
                                    break;
                                case EMAIL:
                                    getView().openEmailPage();
                                    break;
                                default:
                                    getView().notificationMessage(android.R.drawable.ic_dialog_alert,
                                            R.string.confirmation_code_incorrect);
                                    break;
                            }
                        } else {
                            getView().notificationMessage(android.R.drawable.ic_dialog_alert,
                                    R.string.confirmation_code_incorrect);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
