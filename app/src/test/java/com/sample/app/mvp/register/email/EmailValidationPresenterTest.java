package com.sample.app.mvp.register.email;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.model.ConfirmCode;
import com.sample.app.model.ResponseModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.sample.app.mvp.register.email.EmailValidationPresenter.PROFILE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Meda on 5/3/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class EmailValidationPresenterTest {
    private EmailValidationPresenter presenter;
    @Mock
    private EmailValidationView view;
    @Mock
    private ApiService apiService;
    @Mock
    private ResponseModel responseModel;
    @Mock
    private ResponseBody errorBody;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        presenter = new EmailValidationPresenter(view, apiService);
    }


    @Test
    public void submitEmailAddress_SuccessfulResponseWithProfile_OpenProfilePage() throws Exception {
        when(apiService.confirmPhoneNumber(any(ConfirmCode.class)))
                .thenReturn(Observable.just(Response.success(responseModel)));
        when(responseModel.isSuccess())
                .thenReturn(true);
        when(responseModel.getNext())
                .thenReturn(PROFILE);

        presenter.submitEmailAddress("", "", "");
        verify(view).openProfilePage();
    }

    @Test
    public void submitEmailAddress_ErrorResponse_ShowNotificationError() throws Exception {
        when(apiService.confirmPhoneNumber(any(ConfirmCode.class)))
                .thenReturn(Observable.just(Response.error(400, errorBody)));

        presenter.submitEmailAddress("", "", "");
        verify(view).notificationMessage(android.R.drawable.ic_dialog_alert,
                R.string.error);
    }


    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

}