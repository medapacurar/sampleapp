package com.sample.app.mvp.register;

import com.sample.app.R;
import com.sample.app.api.ApiService;
import com.sample.app.model.AppDetails;
import com.sample.app.model.MobileValidation;
import com.sample.app.model.ResponseModel;
import com.sample.app.mvp.register.mobile.MobileValidationPresenter;
import com.sample.app.mvp.register.mobile.MobileValidationView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Meda on 4/20/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class MobileValidationPresenterTest {
    private static final String CONFIRM_CODE = "Test_Confirm_Code";
    private static final String MOBILE_NUMBER = "0746000000";
    private static final String ENCRYPTION_KEY = "Test_Encryption_Key";
    private static final String APP_ID = "Test_App_Id";

    private MobileValidationPresenter presenter;
    @Mock
    private MobileValidationView view;
    @Mock
    private ApiService apiServices;
    @Mock
    private AppDetails appDetails;
    @Mock
    private ResponseBody errorBody;
    @Mock
    private ResponseModel responseModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        presenter = new MobileValidationPresenter(view, apiServices);
        when(view.buildApi()).thenReturn(apiServices);
    }

    @Test
    public void viewDetails_SuccessfulRequestAndNumberRegistered_OpenCodeConfirm() throws Exception {
        when(apiServices.getDetails(MOBILE_NUMBER))
                .thenReturn(Observable.just(Response.success(appDetails)));
        when(appDetails.getConfirmCode())
                .thenReturn(CONFIRM_CODE);
        presenter.viewDetails(MOBILE_NUMBER);
        verify(view).openCodeConfirmation(CONFIRM_CODE);
    }


    @Test
    public void viewDetails_SuccessfulRequestNumberNotRegistered_StoreKeysSubmitNumber() throws Exception {
        when(apiServices.getDetails(MOBILE_NUMBER))
                .thenReturn(Observable.just(Response.success(appDetails)));
        when(appDetails.getEncryptionKey())
                .thenReturn(ENCRYPTION_KEY);
        when(appDetails.getAppId())
                .thenReturn(APP_ID);
        presenter.viewDetails(MOBILE_NUMBER);
        verify(view).storeKeys(ENCRYPTION_KEY, APP_ID);
    }

    @Test
    public void viewDetails_Error_ShowNotificationError() throws Exception {
        when(apiServices.getDetails(MOBILE_NUMBER))
                .thenReturn(Observable.just(Response.error(400, errorBody)));
        presenter.viewDetails(MOBILE_NUMBER);
        verify(view).notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
    }

    @Test
    public void validateTextChanges_CorrectLength_EnableBtn() throws Exception {
        presenter.validateTextChanges("0123456789");
        verify(view).startButtonEnable(true);
    }

    @Test
    public void validateTextChanges_IncorrectLength_DisableBtn() throws Exception {
        presenter.validateTextChanges("012345");
        verify(view).startButtonEnable(false);
    }

    @Test
    public void submitNumber_SuccessfulResponseAndInvalidNextPage_ShowNotificationError() throws Exception {
        when(apiServices.registerPhoneNumber(any(MobileValidation.class)))
                .thenReturn(Observable.just(Response.success(responseModel)));
        when(responseModel.isSuccess())
                .thenReturn(true);
        when(responseModel.getNext())
                .thenReturn("");
        presenter.submitNumber(MOBILE_NUMBER);
        verify(view).notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
    }

    @Test
    public void submitNumber_ErrorResponse_ShowNotificationError() throws Exception {
        when(apiServices.registerPhoneNumber(any(MobileValidation.class)))
                .thenReturn(Observable.just(Response.error(400, errorBody)));
        presenter.submitNumber(MOBILE_NUMBER);
        verify(view).notificationMessage(android.R.drawable.ic_dialog_alert, R.string.error);
    }


    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }
}