package com.sample.app.model;

/**
 * Created by Meda on 4/27/2017.
 */

public class AppDetails {

    private long id;
    private String phoneNumber;
    private String encryptionKey;
    private String confirmCode;
    private String appId;

    public AppDetails(long id, String phoneNumber, String encryptionKey, String confirmCode, String appId) {
        this.id = id;
        this.phoneNumber = phoneNumber != null ? phoneNumber : "";
        this.encryptionKey = encryptionKey != null ? encryptionKey : "";
        this.confirmCode = confirmCode != null ? confirmCode : "";
        this.appId = appId != null ? appId : "";
    }

    public long getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public String getAppId() {
        return appId;
    }
}
