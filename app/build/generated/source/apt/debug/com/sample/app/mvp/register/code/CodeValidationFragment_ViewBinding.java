// Generated code from Butter Knife. Do not modify!
package com.sample.app.mvp.register.code;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sample.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CodeValidationFragment_ViewBinding implements Unbinder {
  private CodeValidationFragment target;

  private View view2131558540;

  @UiThread
  public CodeValidationFragment_ViewBinding(final CodeValidationFragment target, View source) {
    this.target = target;

    View view;
    target.inputCode = Utils.findRequiredViewAsType(source, R.id.code_input, "field 'inputCode'", EditText.class);
    view = Utils.findRequiredView(source, R.id.code_submit, "field 'submit' and method 'submitPressed'");
    target.submit = Utils.castView(view, R.id.code_submit, "field 'submit'", Button.class);
    view2131558540 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submitPressed();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CodeValidationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.inputCode = null;
    target.submit = null;

    view2131558540.setOnClickListener(null);
    view2131558540 = null;
  }
}
